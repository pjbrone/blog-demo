var self = window

var Code = (function (window) {
    return {
        // ToTabs will group all consecutive code blocks and turn them into a
        // tabbed layout
        ToTabs: function () {
            const codeBlocks = this.findConsecutiveBlocks(2)

            const inserted = []
            let bi = 0 // block index
            for (const block of codeBlocks) {
                let ti = 0 // tab index
                let html = `<ul id="tabbed${bi}" class="tabs">`
                for (const tab of block) {
                    const language = tab.className.split('-')[1]
                    const index = `${bi}-${ti}`

                    let checked = ""
                    if (ti == 0) {
                        checked = "checked"
                    }

                    html += `
                        <li class="tab">
                        <input type="radio" name="tabbed${bi}" id="tab${index}" ${checked}/>
                        <label class="tab-label" for="tab${index}">${language}</label>
                        <div id="tab-content-${index}" class="content">${tab.outerHTML}</div>
                        </li>
                    `

                    ti++
                }
                html += '</ul>'

                // insert the tabs just before the first code block
                const tabs = this.htmlToElement(html)
                block[0].parentNode.insertBefore(tabs, block[0])
                inserted.push(tabs)

                // remove the <pre> elements
                for (const tab of block) {
                    tab.parentNode.removeChild(tab)
                }

                bi++
            }

            // add onclick handlers to fix the height when changing tab
            for (const el of window.document.getElementsByClassName('tab-label')) {
                el.onclick = function () {
                    const tabs = el.closest('ul')
                    const id = el.getAttribute("for").slice(3)
                    const content = document.getElementById(`tab-content-${id}`)
                    tabs.style.height = this.getHeight(content)
                }.bind(this)
            }

            for (const block of inserted) {
                const labels = block.getElementsByClassName('tab-label')
                labels[0].click();
            }
        },

        // findConsecutiveBlocks will search for all <pre> tags and return a
        // nested array of consectuive blocks of minLength
        findConsecutiveBlocks: function (minLength) {
            const pres = window.document.getElementsByTagName('pre')

            let blocks = []
            let block = []
            let curr, next;
            for (let i = 0; i < pres.length; i++) {
                curr = pres[i]
                next = pres[i + 1]

                block.push(curr)
                if (curr.nextElementSibling != next) {
                    blocks.push(block)
                    block = []
                }
            }
            blocks.push(block)

            blocks = blocks.filter(el => el.length >= minLength).filter(Boolean)
            return blocks
        },

        // htmlToElement turns an html string into a DOM element
        htmlToElement(html) {
            // TODO: check if browser supports templates
            var template = document.createElement('template');
            html = html.trim();
            template.innerHTML = html;
            return template.content.firstChild;
        },


        getHeight(element) {
            element = element.cloneNode(true);
            element.style.visibility = "hidden";
            document.body.appendChild(element);
            var height = element.offsetHeight;
            document.body.removeChild(element);
            element.style.visibility = "visible";
            return height;
        }
    }
})(self)

export default Code